<!DOCTYPE HTML>
<!--
    Ion by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
    <head>
        <title>Awkwardly Waving Queer</title>
        <base href="https://orsokuma.tk/awq/" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="Title Generator" />
        <meta name="author" content="Magictallguy" />
        <meta name="keywords" content="" />
        <!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
        <script src="js/jquery.min.js"></script>
        <script src="js/skel.min.js"></script>
        <script src="js/skel-layers.min.js"></script>
        <script src="js/init.js"></script>
        <noscript>
            <link rel="stylesheet" href="css/skel.css" />
            <link rel="stylesheet" href="css/style.css" />
            <link rel="stylesheet" href="css/style-xlarge.css" />
        </noscript>
    </head>
    <body id="top">

        <!-- Header -->
            <header id="header" class="skel-layers-fixed">
                <h1><a href="#">AWQ</a></h1>
                <nav id="nav">
                    <ul>
                        <li><a href="index.php">Home</a></li>
                    </ul>
                </nav>
            </header>

        <!-- Banner -->
            <!-- <section id="banner">
                <div class="inner">
                    <h2>This is Awkwardly Waving Queer's Title Generator</h2>
                    <p>A list of titles compiled over time from random guff we've said or heard</p>
                </div>
            </section> -->

        <!-- One -->
            <section id="one" class="wrapper style1">
                <header class="major">
                    <h2>Press the button</h2>
                    <p>It's piss easy</p>
                </header>
                <div class="container">
                    <div class="row">
                        <p>
                            <form action="index.php" method="post">
                                <input type="submit" name="select" value="Pick a title" />
                            </form>
                        </p>
                        <p>
                            <form action="index.php" method="post">
                                <input type="submit" name="generate" value="Generate some trash" />
                            </form>
                        </p>
                        <p><?php
                        $file = __DIR__.'/titles.txt';
                        if ((array_key_exists('select', $_POST) || array_key_exists('generate', $_POST)) && !file_exists($file)) {
                            echo 'The titles are being updated';
                        } else {
                            if (array_key_exists('select', $_POST)) {
                                $f_contents = file($file);
                                $title = $f_contents[array_rand($f_contents)];
                                echo '<br /><h4>'.$title.'</h4>';
                            }
                            if (array_key_exists('generate', $_POST)) {
                                $filecontents = file_get_contents($file);
                                $filebits = explode(' ', str_replace(['(', ')', '-', ',', '?', '!', '.'], '', $filecontents));
                                $wordkeys = array_rand($filebits, mt_rand(3, 10));
                                $words = array();
                                foreach ($wordkeys as $key) {
                                    $words[] = $filebits[$key];
                                }
                                echo '<h4>',ucwords(implode(' ', $words)),'</h4>';
                            }
                        }
                        ?></p>
                    </div>
                </div>
            </section>

        <!-- Footer -->
            <footer id="footer">
                <div class="container">
                    <ul class="copyright">
                        <li>&copy; Awkwardly Waving Queer. All rights ignored.</li>
                        <li>I am not even remotely sorry for this.. ~ Bear</li>
                    </ul>
                </div>
            </footer>

    </body>
</html>
